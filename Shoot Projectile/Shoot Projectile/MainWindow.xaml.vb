﻿Imports System.Windows.Threading

Class MainWindow
    Private upKey As Boolean
    Private downKey As Boolean
    Dim leftKey As Boolean
    Dim rightKey As Boolean
    Dim speed As Integer = 10
    Dim newShipPosition As Integer
    Private newShipLeft As Integer
    Private newShipRight As Double

    Private src As BitmapImage
    Private laser As Image

    Private srcShip As BitmapImage
    Private imgShip As Image

    Private laserSpeed As Integer
    Private isFireReady As Boolean
    Private laserTimer As DispatcherTimer
    Private newLaserPosition As Double
    Private offsetLaser As Double

    Enum MoveKey
        Left = 0
        Right = 1
        Up = 2
        Down = 3
        Fire = 4
    End Enum

    Sub New()



        InitializeComponent()
        'CGame Loop
        AddHandler CompositionTarget.Rendering, AddressOf GameLoop

        'Set image bmp
        src = New BitmapImage()
        src.BeginInit()
        src.UriSource = New Uri("images/laser.png", UriKind.RelativeOrAbsolute)
        src.CacheOption = BitmapCacheOption.OnLoad
        src.EndInit()

        'Set image bmp
        srcShip = New BitmapImage()
        srcShip.BeginInit()
        srcShip.UriSource = New Uri("images/ship.png", UriKind.RelativeOrAbsolute)
        srcShip.CacheOption = BitmapCacheOption.OnLoad
        srcShip.EndInit()



        laser = New Image()
        imgShip = New Image With {
            .Source = srcShip,
            .Stretch = Stretch.Uniform,
            .Width = 100,
            .Height = 100
        }


        Canvas.SetLeft(imgShip, 200)
        Canvas.SetTop(imgShip, 200)
        mainCanvas.Children.Add(imgShip)

        laserSpeed = 5
        isFireReady = True
        offsetLaser = 20
    End Sub



    Private Sub GameLoop()


        If leftKey Then CheckKeyToMove(MoveKey.Left)
        If rightKey Then CheckKeyToMove(MoveKey.Right)
        If upKey Then CheckKeyToMove(MoveKey.Up)
        If downKey Then CheckKeyToMove(MoveKey.Down)





    End Sub
    Private Sub shipGame_KeyDown(sender As Object, e As KeyEventArgs) Handles shipGame.KeyDown
        Select Case e.Key
            Case Key.Right
                rightKey = True
            Case Key.Left
                leftKey = True
            Case Key.Up
                upKey = True
            Case Key.Down
            Case Else
        End Select

    End Sub
    Private Sub CheckKeyToMove(isKeyMove As MoveKey)
        Select Case isKeyMove
            Case isKeyMove.Left
                MoveLeft()
            Case isKeyMove.Right
                MoveRight()

            Case isKeyMove.Up
                MoveUp()
            Case isKeyMove.Down
                MoveDown()
            Case Else
        End Select
        'Canvas.SetLeft(imgShip, mainCanvas.Width)
        'Canvas.SetTop(imgShip, mainCanvas.Height)
    End Sub

    Private Sub MoveRight()
        newShipRight = Canvas.GetLeft(imgShip)
        newShipRight += speed
        Canvas.SetLeft(imgShip, newShipRight)
    End Sub

    Private Sub MoveLeft()
        newShipRight = Canvas.GetLeft(imgShip)
        newShipRight -= speed
        Canvas.SetLeft(imgShip, newShipRight)
    End Sub
    Private Sub MoveUp()

        '  rectShip.Y -= speed
    End Sub

    Private Sub MoveDown()
        ' rectShip.Y += speed
    End Sub

    Private Sub shipGame_KeyUp(sender As Object, e As KeyEventArgs) Handles shipGame.KeyUp
        Select Case e.Key
            Case Key.Right
                rightKey = False
            Case Key.Left
                leftKey = False
            Case Key.Up
                upKey = False
            Case Key.Down
                downKey = False
            Case Key.Space
                If isFireReady Then
                    makeLaser(imgShip, laser)
                    FireLaser()
                    isFireReady = False
                End If

            Case Else
        End Select
    End Sub

    Private Sub makeLaser(startObject As Image, laser As Image)
        laser.Source = src
        laser.Stretch = Stretch.Uniform

        laser.Width = 40
        laser.Height = 40

        '  Dim RotateTransform = New RotateTransform(90)
        ' laser.RenderTransform = RotateTransform


        Canvas.SetLeft(laser, Canvas.GetLeft(startObject) + ((startObject.Width / 2) - offsetLaser))
        Canvas.SetTop(laser, Canvas.GetTop(startObject))

        mainCanvas.Children.Add(laser)
    End Sub

    Private Sub FireLaser()

        laserTimer = New DispatcherTimer()
        AddHandler laserTimer.Tick, AddressOf laserTimer_Tick
        laserTimer.Interval = TimeSpan.FromMilliseconds(speed)

        laserTimer.Start()


    End Sub

    Private Sub laserTimer_Tick(sender As Object, e As EventArgs)


        newLaserPosition = (Canvas.GetTop(laser) - laserSpeed)
        Canvas.SetTop(laser, newLaserPosition)


        If newLaserPosition < (0 - imgShip.Height) Then
            Console.WriteLine("done")
            laserTimer.Stop()
            mainCanvas.Children.Remove(laser)
            isFireReady = True
        Else

            Console.WriteLine("running")
        End If

    End Sub
End Class
